-module(manager).
-export([new/0, add/3, add/4, update/3, remove/2, find/4, do_when/3, do_when/4, remove_when/4, map/2, stop/1]).

new() ->
  spawn_link(fun() -> loop(maps:new()) end).

loop(Stuff) ->
  receive
    {add, Name, Whatever} ->
      loop(Stuff#{Name => Whatever});
    {add, Name, Whatever, F} ->
      spawn(F),
      loop(Stuff#{Name => Whatever});
    {update, Name, UpdateFunction} ->
      loop(Stuff#{Name => UpdateFunction(maps:get(Name, Stuff))});
    {remove, Name} ->
      loop(maps:remove(Name, Stuff));
    {find, Name, From, To} ->
      From ! {find_result, maps:find(Name, Stuff), To},
      loop(Stuff);
    {map, F} ->
      loop(maps:map(F, Stuff));
    {do_when, Name, Success, Failure} ->
      case maps:find(Name, Stuff) of
        {ok, Value} ->
          Success(Value);
        error ->
          Failure()
      end,
      loop(Stuff);
    {remove_when, Name, Success, Failure} ->
      case maps:find(Name, Stuff) of
        {ok, Value} ->
          Success(Value),
          loop(maps:remove(Name, Stuff));
        error ->
          Failure(),
          loop(Stuff)
      end;
    stop ->
      ok;
    Error ->
      io:format("An error has ocurred in servers ~w~n", [Error])
  end.

add(Manager, Name, Whatever) ->
  Manager ! {add, Name, Whatever}.

add(Manager, Name, Whatever, F) ->
  Manager ! {add, Name, Whatever, F}.

update(Manager, Name, UpdateFunction) ->
  Manager ! {update, Name, UpdateFunction}.

remove(Manager, Name) ->
  Manager ! {remove, Name}.

find(Manager, Name, From, To) ->
  Manager ! {find, Name, From, To}.

do_when(Manager,  Name, Success, Failure) ->
  Manager ! {do_when, Name, Success, Failure}.

do_when(Manager,  Name, Success) ->
  Manager ! {do_when, Name, Success, fun() -> none end}.

remove_when(Manager,  Name, Success, Failure) ->
  Manager ! {remove_when, Name, Success, Failure}.

map(Manager,  F) ->
  Manager ! {map, F}.

stop(Manager) ->
  Manager ! stop.
