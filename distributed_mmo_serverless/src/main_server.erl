-module(main_server).
-export([start/0, add_zone_server/3, connect_event/3, register_event/4, stop/1]).

start() ->
  PID = spawn_link(fun() -> init() end),
  register(main_server, PID),
  PID.

init() -> loop(manager:new(), manager:new()).

loop(Servers, Players) ->
  receive
    stop ->
      manager:stop(Servers),
      manager:stop(Players);
    { add_zone_server, Name, Server } ->
      manager:add(Servers, Name, Server),
      loop(Servers, Players);
    { connect_event , Name, PID } ->
      manager:do_when(Players, Name,
        fun(Server) -> connect(Servers, self(), PID, Name, Server) end ,
        fun() -> handle_error(PID) end
        ),
      loop(Servers, Players);
    { register_event , Name, PID, Server } ->
      manager:do_when(Players, Name,
        fun (_) -> PID ! name_already_taken end,
        fun () -> manager:do_when(Servers, Server,
          fun (ServerPID) -> manager:add(Players, Name, Server), ServerPID ! {register_event, Name, PID} end,
          fun () -> PID ! server_does_not_exist
          end)
        end),
      loop(Servers, Players);
    { disconnect_player, Name, LastServer } ->
      io:format("ms: desconectando a ~w en el server ~w~n",[Name,LastServer]),
      manager:add(Players, Name, LastServer),
      loop(Servers, Players)
  end.

add_zone_server(MainServer, Name, Server) -> MainServer ! {add_zone_server, Name, Server}.

connect_event(MainServer, Name, PID) -> MainServer ! {connect_event, Name, PID}.

register_event(MainServer, Name, PID, Server) -> MainServer ! {register_event, Name, PID, Server}.

stop(MainServer) -> MainServer ! stop.

handle_error({_, PID}) -> PID ! unauthorized;
handle_error(_) -> bad_request.


connect(Servers, _From, To, Name, Server) ->
  manager:do_when(Servers, Server,
  fun(ServerPID) -> ServerPID ! {connect_event, Name, To} end,
  fun() -> To ! server_does_not_exist end).
