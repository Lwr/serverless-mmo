-module(game_state).
-export([start/3, start/7]).

-include_lib("types.hrl").

start(Name, Row, Column) ->
  PID = spawn(fun() -> init(Name, Row, Column) end),
  {W,_} = Row,
  {Y, _} = Column,
  register(list_to_atom("game_state_" ++
                        lists:flatten(io_lib:format("~p", [W])) ++
                        "_" ++
                        lists:flatten(io_lib:format("~p", [Y]))), PID),
  PID.

start(SName, {W, X}, {Y, Z}, State, Suscriptors, Transferred, Items) ->
  lists:foreach(fun({S, _, _}) -> get_pid(S) ! {new_server, SName, self()} end,
  lists:filter(fun({S, _Row, _Column}) -> is_record(S, server) end, maps:values(Suscriptors))),
  lists:foreach(fun(X) -> get_pid(X) ! {reconnect, self(), X, State} end, maps:values(State)),
  SName ! {set_state, self()},
  loop(SName, {W, X}, {Y, Z}, State, Suscriptors, Transferred, Items, []).

init(Name, {W, X}, {Y, Z}) ->
  receive
     {slave, Slave} ->
      receive
        {neighbours, Neighbours} ->% list of {PID, Row, Column} interested for every PID
          lists:foreach(fun({PID, Row, Column}) -> PID ! {suscribe, mk_server(Name, self(), {W, X}, {Y, Z}), Row, Column} end,Neighbours),
          Items = manager:new(),
          Item1 = mk_item({random:uniform(X-W)+W,random:uniform(Z-Y)+Y}, 3),
          manager:add(Items, Item1#item.name, Item1),
          Item2 = mk_item({random:uniform(X-W)+W,random:uniform(Z-Y)+Y}, 3),
          manager:add(Items, Item2#item.name, Item2),
          Item3 = mk_item({random:uniform(X-W)+W,random:uniform(Z-Y)+Y}, 3),
          manager:add(Items, Item3#item.name, Item3),
          loop(Name, {W, X}, {Y, Z}, maps:new(), maps:new(), maps:new(), Items, [Slave])
      end;
    {neighbours, Neighbours} ->% list of {PID, Row, Column} interested for every PID
        lists:foreach(fun({PID, Row, Column}) -> PID ! {suscribe, mk_server(Name, self(), {W, X}, {Y, Z}), Row, Column} end,Neighbours),
        Items = manager:new(),
        Item1 = mk_item({random:uniform(X-W)+W,random:uniform(Z-Y)+Y}, 3),
        manager:add(Items, Item1#item.name, Item1),
        Item2 = mk_item({random:uniform(X-W)+W,random:uniform(Z-Y)+Y}, 3),
        manager:add(Items, Item2#item.name, Item2),
        Item3 = mk_item({random:uniform(X-W)+W,random:uniform(Z-Y)+Y}, 3),
        manager:add(Items, Item3#item.name, Item3),
        loop(Name, {W, X}, {Y, Z}, maps:new(), maps:new(), maps:new(), Items, [])
  end.

loop(SName, {W, X}, {Y, Z}, State, Suscriptors, Transferred, Items, Slaves) ->
  receive
    % as a server
    {connect_event, Player} ->
      lists:foreach(fun(Slave) ->  Slave ! {connect_event, Player} end, Slaves),
      {State2, Suscriptors2} = handle_connect_event(Player, SName, {W,X}, {Y,Z}, State, Suscriptors, Transferred),
      manager:map(Items, fun (Name, Item) -> get_pid(Player) ! {new_item, Item}, Item end),
      loop(SName, {W, X}, {Y, Z}, State2, Suscriptors2, Transferred, Items, Slaves);
    {new_item_event, Item} ->
      lists:foreach(fun(Slave) -> Slave ! {new_item_event, Item} end, Slaves),
      handle_new_item_event(SName, Suscriptors, Items, Item),
      loop(SName, {W, X}, {Y, Z}, State, Suscriptors, Transferred, Items, Slaves);
    {suscribe, Suscriptor, {PW, PX}, {PY, PZ}} ->
      lists:foreach(fun(Slave) -> Slave ! {suscribe, Suscriptor, {PW, PX}, {PY, PZ}} end, Slaves),
      InterestedRow = {max(PW, W), min(X, PX)},
      InterestedColumn = {max(PY, Y), min(Z, PZ)},
      loop(SName, {W, X}, {Y, Z}, State,
      maps:put(get_name(Suscriptor), {Suscriptor, InterestedRow, InterestedColumn}, Suscriptors), Transferred, Items, Slaves);
    {new_pos_event, Name, OldPos, NewPos} ->
      lists:foreach(fun(Slave) -> Slave ! {new_pos_event, Name, OldPos, NewPos} end, Slaves),
      {State2, Suscriptors2, Transferred2} = handle_new_pos_event(Name, OldPos, NewPos, SName, {W,X}, {Y,Z}, State, Suscriptors, Transferred, Items),
      loop(SName, {W,X}, {Y,Z}, State2, Suscriptors2, Transferred2, Items, Slaves);
    {transfer_event, _Pid, Obj, OldPos, NewPos} ->
      lists:foreach(fun(Slave) -> Slave ! {transfer_event, _Pid, Obj, OldPos, NewPos} end, Slaves),
      {State2, Suscriptors2} = handle_transfer_event(Obj, OldPos, NewPos, SName, {W,X}, {Y,Z}, State, Suscriptors, Items),
      loop(SName, {W, X}, {Y, Z}, State2, Suscriptors2, Transferred, Items, Slaves);
    {ack_transfer, Name, LastServer} ->
      lists:foreach(fun(Slave) -> Slave ! {ack_transfer, Name, LastServer} end, Slaves),
      LastServer ! {stop_forwarding, Name},
      loop(SName, {W, X}, {Y, Z}, State, Suscriptors, Transferred, Items, Slaves);
    {stop_forwarding, Name} ->
      lists:foreach(fun(Slave) -> Slave ! {stop_forwarding, Name} end, Slaves),
      loop(SName, {W, X}, {Y, Z}, State, Suscriptors, maps:remove(Name, Transferred), Items, Slaves);
    {disconnect_event, Name} ->
      lists:foreach(fun(Slave) ->       Slave ! {disconnect_event, Name} end, Slaves),
      SName ! {disconnect_event, maps:get(Name,State)},
      {State2, Suscriptors2} = handle_disconnect_event(maps:get(Name,State), State, Suscriptors),
      loop(SName, {W, X}, {Y, Z}, State2, Suscriptors2, Transferred, Items, Slaves);
    {pick_up_event, Name, IName} ->
      lists:foreach(fun(Slave) ->   Slave ! {pick_up_event, Name, IName} end, Slaves),
      PID = self(),
      Player = maps:get(Name, State),
      manager:remove_when(Items, IName, fun(Item) -> PID ! {actual_pick_up, Name, Item}, get_pid(Player) ! {picked_up, Item} end, fun() -> get_pid(Player) ! item_already_taken end),
      loop(SName, {W, X}, {Y, Z}, State, Suscriptors, Transferred, Items, Slaves);
    {actual_pick_up, Name, Item} ->
      lists:foreach(fun(Slave) ->       Slave ! {actual_pick_up, Name, Item} end, Slaves),

      Player = maps:get(Name, State),
      maps:map(fun(_K, {Suscriptor, Row, Column}) ->
        case is_intereseted(get_position(Item), Row, Column) of
          true ->
            get_pid(Suscriptor) ! {out_of_vision, get_name(Item), get_position(Item)};
          false ->
            none
        end
      end, Suscriptors),
      loop(SName, {W, X}, {Y, Z}, State#{Name => (Player#player{items= [Item|Player#player.items]})}, Suscriptors, Transferred, Items, Slaves);
    % as a client
    {meet_player, Player} ->
      lists:foreach(fun(Slave) -> Slave ! {meet_player, Player}  end, Slaves),
      Suscriptors2 = update_interest(get_name(Player), Player, {W,X}, {Y,Z}, Suscriptors),
      loop(SName, {W, X}, {Y, Z}, State, Suscriptors2, Transferred, Items, Slaves);
    {out_of_vision, Name, _OldPos} ->
      lists:foreach(fun(Slave) ->  Slave ! {out_of_vision, Name, _OldPos} end, Slaves),
      loop(SName, {W, X}, {Y, Z}, State, maps:remove(Name, Suscriptors), Transferred, Items, Slaves);
    {change_pos, Name, Obj, OldPos, _NewPos, _Pid} when is_record(Obj, player) ->
      lists:foreach(fun(Slave) -> Slave ! {change_pos, Name, Obj, OldPos, _NewPos, _Pid} end, Slaves),
      Suscriptors2 = update_interest(Name, Obj,{W,X},{Y,Z}, Suscriptors),
      handle_items(change_position(Obj, OldPos), Obj, {W,X},{Y,Z}, Items),
      loop(SName, {W, X}, {Y, Z}, State, Suscriptors2, Transferred, Items, Slaves);
    {change_pos, _Name, Obj, OldPos, NewPos, _Pid} ->
      lists:foreach(fun(Slave) -> Slave ! {change_pos, _Name, Obj, OldPos, NewPos, _Pid}  end, Slaves),
      loop(SName, {W, X}, {Y, Z}, State, Suscriptors, Transferred, Items, Slaves);
    {new_item, Item} ->
      lists:foreach(fun(Slave) -> Slave ! {new_item, Item}  end, Slaves),
      loop(SName, {W, X}, {Y, Z}, State, Suscriptors, Transferred, Items, Slaves);
    {new_server, ServerPid, Pid} ->
      ServerName = element(1, ServerPid),
      lists:foreach(fun(Slave) ->  Slave ! {new_server, ServerPid, Pid} end, Slaves),
      {Server, Row, Column} = maps:get(ServerName, Suscriptors),
      loop(SName, {W, X}, {Y, Z}, State, Suscriptors#{ServerName => {Server#server{pid= Pid}, Row,Column}}, Transferred, Items, Slaves);
    {disconnect_player, Name} ->
      lists:foreach(fun(Slave) -> Slave ! {disconnect_player, Name} end, Slaves),
      State2 = maps:remove(Name, State),
      Suscriptors2 = maps:remove(Name, Suscriptors),
      Transferred2 = maps:remove(Name, Transferred),
      loop(SName, {W, X}, {Y, Z}, State2, Suscriptors2, Transferred2, Items, Slaves);
    stop ->
      lists:foreach(fun(Slave) -> Slave ! stop end, Slaves),
      handle_stop(SName, Suscriptors, Transferred),
      ok
  end.
% ------------------ AGREGANDO PARA APAGAR UN SERVER ------------------
handle_stop(SName, Suscriptors, Transferred) ->
  ok.

handle_disconnect_event(Player, State, Suscriptors) ->
  State1 = notify_disconnect_player(Player, State, maps:values(Suscriptors)),
  State2 = remove_player(Player, State1),
  Suscriptors2 = remove_player(Player, Suscriptors),
  {State2, Suscriptors2}.

notify_disconnect_player(Player, State, []) -> State;
notify_disconnect_player(Player, State, [{Suscriptor, Row, Column} | Suscriptors]) ->
  case is_record(Suscriptor, player) of
    true ->
      case know_him(Suscriptor, Player) of
        true ->
          State2 = ignore_player(Suscriptor, Player, State),
          notify_disconnect_player(Player, State2, Suscriptors);
        false ->
          notify_disconnect_player(Player, State, Suscriptors)
      end;
    false ->
      get_pid(Suscriptor) ! {disconnect_player, get_name(Player)},
      notify_disconnect_player(Player, State, Suscriptors)
    end.

ignore_player(Player1, Player2, State) ->
  case get_name(Player1) == get_name(Player2) of
    true ->
      State;
    false ->
      State2 = maps:put(get_name(Player1), ignore(Player1, Player2), State),
      get_pid(Player1) ! {out_of_vision, get_name(Player2), get_position(Player2)},
      State2
  end.

ignore(Player1, Player2) ->
  Player1#player{known_pids = maps:remove(get_name(Player2), Player1#player.known_pids)}.

remove_player(Player, State) ->
  maps:remove(get_name(Player), State).

update_knows(Player, Name, Pid) ->
  Player#player{known_pids = maps:put(Name, Pid, Player#player.known_pids)}.

know_him(Player1, Player2) ->
  maps:is_key(get_name(Player2), Player1#player.known_pids).


% ------------------ AGREGANDO PARA APAGAR UN SERVER ------------------

update_interest(Name, Obj,{W,X},{Y,Z}, Suscriptors) ->
    {InterestedRow, InterestedColumn} = calculate_interest(Obj, {W, X}, {Y, Z}),
    maps:put(Name, {Obj, InterestedRow, InterestedColumn}, Suscriptors).

handle_connect_event(Player, SName, {W,X}, {Y,Z}, State, Suscriptors, Transferred) ->
  {UpdatedState, UpdatedPlayer} = add_player(SName, Player, State),
  get_pid(Player) ! {ack_connect, UpdatedState, self(), UpdatedPlayer#player.position},
  State2 = notify_new_player(UpdatedPlayer, UpdatedState, maps:values(Suscriptors)),
  Suscriptors2 = update_interest(get_name(UpdatedPlayer), UpdatedPlayer, {W,X}, {Y,Z}, Suscriptors),
  {State2, Suscriptors2}.

handle_new_item_event(SName, Suscriptors, Items, Item) ->
  manager:add(Items, get_name(Item), Item,
  fun() ->
    maps:map(fun(_K, S) -> get_pid(S) ! {new_item, Item} end, Suscriptors)
  end).

handle_new_pos_event(Name, OldPos, NewPos, SName, {W,X}, {Y,Z}, State, Suscriptors, Transferred, Items) ->
  case need_forward(Name, Transferred) of
    true ->
      Serv = maps:get(Name, Transferred),
      case is_record(Serv, server) of
        true ->
          get_pid(maps:get(Name, Transferred)) ! {new_pos_event, Name, OldPos, NewPos};
        false ->
          none
      end,
      {State, Suscriptors, Transferred};
    false ->
      case maps:find(Name, State) of
        {ok, Obj} ->
          UpdatedState = maps:remove(Name, State),
          case is_intereseted(NewPos, {W, X}, {Y, Z}) of
            true ->
              {NewObj, State2, Suscriptors2} = change_position_workflow(Obj, OldPos, NewPos, {W,X}, {Y,Z}, State, Suscriptors, Items),
              {State2, Suscriptors2, Transferred};
            false ->
              IServer = select_transfer_event_server(Obj, OldPos, NewPos, maps:values(Suscriptors)),
              Transferred2 = Transferred#{get_name(Obj) => IServer},
              {UpdatedState, Suscriptors, Transferred2}
          end;
        error ->
          io:format("~w no lo encontre! ~w~n", [SName, Name]),
          {State, Suscriptors, Transferred}
      end
  end.

handle_transfer_event(Obj, OldPos, NewPos, SName, {W,X}, {Y,Z}, State, Suscriptors, Items) ->
  {NewObj, State2, Suscriptors2} = change_position_workflow(Obj, OldPos, NewPos,{W,X},{Y,Z}, State, Suscriptors, Items),
  get_pid(NewObj) ! {new_server, self()},
  {State2, Suscriptors2}.

change_position_workflow(Obj, OldPos, NewPos, {W,X}, {Y,Z}, State, Suscriptors, Items) ->
  NewObj = change_position(Obj, NewPos),
  State2 = State#{get_name(NewObj) => NewObj},
  NewState = notify_pos_update_with(State2, NewObj, OldPos, NewPos, maps:values(Suscriptors)),
  Suscriptors2 = update_interest(get_name(NewObj), NewObj, {W, X}, {Y, Z}, Suscriptors),
  handle_items(Obj, NewObj, {W,X},{Y,Z}, Items),
  {NewObj, NewState, Suscriptors2}.

handle_items(Obj, NewObj, {W,X},{Y,Z}, Items) when is_record(Obj, player) ->
  {Row, Column} = calculate_interest(NewObj, {W,X}, {Y,Z}),
  {OldRow, OldColumn} = calculate_interest(Obj, {W,X}, {Y,Z}),
  manager:map(Items, fun(IName, Item) ->
    case is_intereseted(get_position(Item), Row, Column) andalso
      not is_intereseted(get_position(Item), OldRow, OldColumn) of
      true -> get_pid(NewObj) ! {new_item, Item},
              Item;
      false -> case not is_intereseted(get_position(Item), Row, Column) andalso
        is_intereseted(get_position(Item), OldRow, OldColumn) of
          true ->
            get_pid(NewObj) ! {out_of_vision, IName},
            Item;
          false ->
            Item
        end
    end
  end);
handle_items(_,_,_,_,_) ->
  none.

select_transfer_event_server(Obj, OldPos, NewPos, []) -> none;
select_transfer_event_server(Obj, OldPos, NewPos, [{Suscriptor, Row, Column} | Suscriptors]) ->
  case is_record(Suscriptor, server) andalso is_intereseted(NewPos, Suscriptor#server.row, Suscriptor#server.column) of
    true ->
      get_pid(Suscriptor) ! {transfer_event, self(), Obj, OldPos, NewPos},
      Suscriptor;
    false ->
      select_transfer_event_server(Obj, OldPos, NewPos, Suscriptors)
  end.
meet_player(Player, Player2, State) ->
  P = update_knows(Player, get_name(Player2), get_pid(Player2)),
  State2 = maps:put(get_name(Player), P, State),
  get_pid(P) ! {meet_player, Player2},
  State2.

add_player(SName, Player, State) ->
  P1 = update_knows(Player, get_name(Player), get_pid(Player)),
  UpdatedPlayer = update_knows(P1, SName, self()),
  UpdatedState = State#{get_name(UpdatedPlayer) => UpdatedPlayer},
  {UpdatedState, UpdatedPlayer}.

notify_new_player(Player, State, []) -> State;
notify_new_player(Player, State, [{Suscriptor, Row, Column} | Suscriptors])->
  case is_intereseted(Player#player.position, Row, Column) of
    true ->
      case is_record(Suscriptor, player) of
        true ->
          State2 = double_meeting(Suscriptor, Player, State),
          notify_new_player(Player, State2, Suscriptors);
        false ->
          get_pid(Suscriptor) ! {meet_player, Player},
          notify_new_player(Player, State, Suscriptors)
      end;
    false ->
      notify_new_player(Player, State, Suscriptors)
  end.

double_meeting(Player, Player2, State) ->
  State2 = meet_player(Player, Player2, State),
  meet_player(Player2, maps:get(Player#player.name, State2), State2).

notify_pos_update_with(State, _Obj, _OldPos, _NewPos, []) -> State;
notify_pos_update_with(State, Obj, OldPos, NewPos, [{Suscriptor, Row, Column}|Suscriptors]) ->
  case is_intereseted(NewPos, Row, Column) of
    true ->
      case is_record(Suscriptor, player) andalso is_record(Obj, player) andalso
        not know_him(Obj, Suscriptor) of
        true ->
          NewState = double_meeting(Suscriptor, Obj, State),
          notify_pos_update_with(NewState, Obj, OldPos, NewPos, Suscriptors);
        false ->
          get_pid(Suscriptor) ! {change_pos, get_name(Obj), Obj, OldPos, NewPos, self()},
          notify_pos_update_with(State, Obj, OldPos, NewPos, Suscriptors)
      end;
    false when is_record(Obj, player) ->
      out_of_vision(Obj, Suscriptor),
      case is_intereseted(OldPos, Row, Column) of
        true ->
          get_pid(Suscriptor) ! {out_of_vision, get_name(Obj), OldPos},
          notify_pos_update_with(State, Obj, OldPos, NewPos, Suscriptors);
        false ->
          notify_pos_update_with(State, Obj, OldPos, NewPos, Suscriptors)
      end;
    false ->
      case is_intereseted(OldPos, Row, Column) of
        true ->
          get_pid(Suscriptor) ! {out_of_vision, get_name(Obj), OldPos},
          notify_pos_update_with(State, Obj, OldPos, NewPos, Suscriptors);
        false ->
          notify_pos_update_with(State, Obj, OldPos, NewPos, Suscriptors)
      end
  end.

out_of_vision(#player{pid= Pid}, #server{}) ->
  none;
out_of_vision(#player{pid= Pid}, Suscriptor) ->
  Pid ! {out_of_vision, get_name(Suscriptor), none};
out_of_vision(_Obj, Suscriptor) ->
  none.

need_forward(Name, Map) ->
  maps:is_key(Name, Map).

calculate_interest(Player, {W, X}, {Y, Z}) ->
  {PX, PY} = Player#player.position,
  InterestedRow = {max(PX - Player#player.visibility, W), min(PX + Player#player.visibility, X)},
  InterestedColumn = {max(PY - Player#player.visibility, Y), min(PY + Player#player.visibility, Z)},
  {InterestedRow, InterestedColumn}.

is_intereseted({A, B}, Row, Column) ->
  in(Row, A) andalso in(Column, B).

part_of(Row, Column, Visibility, {W, X}, {Y, Z}) ->
  in_server({Row - Visibility, Row + Visibility + 1}, {Column - Visibility, Column + Visibility + 1}, {W, X}, {Y, Z}).

in_server({W, X}, {Y, Z}, {WP, XP}, {YP, ZP}) ->
  (in({W, X}, WP) andalso (in({Y, Z}, XP) orelse in({Y, Z}, ZP))) orelse
  (in({W, X}, YP) andalso (in({Y, Z}, XP) orelse in({Y, Z}, ZP))).

in({X, Y}, Z) ->
  X =< Z andalso Z < Y.
