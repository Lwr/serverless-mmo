-module(game_state_slave).
-export([start/0, start/4]).

-include_lib("types.hrl").

start() ->
  spawn(fun() -> init() end).

start(Master, Name, Row, Column) ->
  spawn(fun() -> init(Master, Name, Row, Column) end).

init() ->
  receive
    {master, Master, Name, Row, Column} ->
      Ref = erlang:monitor(process, Master),
      Master ! {slave, self()},
      loop(Name, Row, Column, maps:new(), maps:new(), maps:new(), manager:new(), Ref)
  end.

init(Master, Name, Row, Column) ->
  Ref = erlang:monitor(process, Master),
  Master ! {slave, self()},
  loop(Name, Row, Column, maps:new(), maps:new(), maps:new(), manager:new(), Ref).

loop(SName, {W, X}, {Y, Z}, State, Suscriptors, Transferred, Items, Ref) ->
  receive
    % as a server
    {connect_event, Player} ->
      {State2, Suscriptors2} = handle_connect_event(Player, SName, {W,X}, {Y,Z}, State, Suscriptors, Transferred),
      loop(SName, {W, X}, {Y, Z}, State2, Suscriptors2, Transferred, Items, Ref);
    {new_item_event, Item} ->
      handle_new_item_event(SName, Suscriptors, Items, Item),
      loop(SName, {W, X}, {Y, Z}, State, Suscriptors, Transferred, Items, Ref);
    {suscribe, Suscriptor, {PW, PX}, {PY, PZ}} ->
      InterestedRow = {max(PW, W), min(X, PX)},
      InterestedColumn = {max(PY, Y), min(Z, PZ)},
      loop(SName, {W, X}, {Y, Z}, State,
      maps:put(get_name(Suscriptor), {Suscriptor, InterestedRow, InterestedColumn}, Suscriptors), Transferred, Items, Ref);
    {new_pos_event, Name, OldPos, NewPos} ->
      {State2, Suscriptors2, Transferred2} = handle_new_pos_event(Name, OldPos, NewPos, SName, {W,X}, {Y,Z}, State, Suscriptors, Transferred, Items),
      loop(SName, {W,X}, {Y,Z}, State2, Suscriptors2, Transferred2, Items, Ref);
    {transfer_event, _Pid, Obj, OldPos, NewPos} ->
      {State2, Suscriptors2} = handle_transfer_event(Obj, OldPos, NewPos, SName, {W,X}, {Y,Z}, State, Suscriptors, Items),
      loop(SName, {W, X}, {Y, Z}, State2, Suscriptors2, Transferred, Items, Ref);
    {ack_transfer, Name, LastServer} ->
      loop(SName, {W, X}, {Y, Z}, State, Suscriptors, Transferred, Items, Ref);
    {stop_forwarding, Name} ->
      % io:format("~w dejando de forwardear a: ~w~n", [SName, Name]),
      loop(SName, {W, X}, {Y, Z}, State, Suscriptors, maps:remove(Name, Transferred), Items, Ref);
    {disconnect_event, Name} ->
      {State2, Suscriptors2} = handle_disconnect_event(maps:get(Name,State), State, Suscriptors),
      loop(SName, {W, X}, {Y, Z}, State2, Suscriptors2, Transferred, Items, Ref);
    {pick_up_event, Name, IName} ->
      PID = self(),
      Player = maps:get(Name, State),
      manager:remove_when(Items, IName, fun(Item) -> PID ! {actual_pick_up, Name, Item} end, fun() -> none end),
      loop(SName, {W, X}, {Y, Z}, State, Suscriptors, Transferred, Items, Ref);
    {actual_pick_up, Name, Item} ->
      Player = maps:get(Name, State),
      loop(SName, {W, X}, {Y, Z}, State#{Name => (Player#player{items= [Item|Player#player.items]})}, Suscriptors, Transferred, Items, Ref);
    % as a client
    {meet_player, Player} ->
      Suscriptors2 = update_interest(get_name(Player), Player, {W,X}, {Y,Z}, Suscriptors),
      loop(SName, {W, X}, {Y, Z}, State, Suscriptors2, Transferred, Items, Ref);
    {out_of_vision, Name, _OldPos} ->
      loop(SName, {W, X}, {Y, Z}, State, maps:remove(Name, Suscriptors), Transferred, Items, Ref);
    {change_pos, Name, Obj, OldPos, _NewPos, _Pid} when is_record(Obj, player) ->
      Suscriptors2 = update_interest(Name, Obj,{W,X},{Y,Z}, Suscriptors),
      handle_items(change_position(Obj, OldPos), Obj, {W,X},{Y,Z}, Items),
      loop(SName, {W, X}, {Y, Z}, State, Suscriptors2, Transferred, Items, Ref);
    {change_pos, _Name, Obj, OldPos, NewPos, _Pid} ->
      loop(SName, {W, X}, {Y, Z}, State, Suscriptors, Transferred, Items, Ref);
    {new_item, Item} ->
      loop(SName, {W, X}, {Y, Z}, State, Suscriptors, Transferred, Items, Ref);
    {new_server, ServerPid, Pid} ->
      ServerName = element(1, ServerPid),
      {Server, Row, Column} = maps:get(ServerName, Suscriptors),
      loop(SName, {W, X}, {Y, Z}, State, Suscriptors#{ServerName => {Server#server{pid= Pid}, Row,Column}}, Transferred, Items, Ref);
    {disconnect_player, Name} ->
      State2 = maps:remove(Name, State),
      Suscriptors2 = maps:remove(Name, Suscriptors),
      Transferred2 = maps:remove(Name, Transferred),
      loop(SName, {W, X}, {Y, Z}, State2, Suscriptors2, Transferred2, Items, Ref);
    stop ->
      handle_stop(SName, Suscriptors, Transferred),
      ok;
    {'DOWN', Ref, process, Pid, Reason} ->
      io:format("se rompio ~w~n", [SName]),
      game_state:start(SName, {W, X}, {Y, Z}, State, Suscriptors, Transferred, Items)
  end.
% ------------------ AGREGANDO PARA APAGAR UN SERVER ------------------
handle_stop(SName, Suscriptors, Transferred) ->
  ok.

handle_disconnect_event(Player, State, Suscriptors) ->
  State1 = notify_disconnect_player(Player, State, maps:values(Suscriptors)),
  State2 = remove_player(Player, State1),
  Suscriptors2 = remove_player(Player, Suscriptors),
  {State2, Suscriptors2}.

notify_disconnect_player(Player, State, []) -> State;
notify_disconnect_player(Player, State, [{Suscriptor, Row, Column} | Suscriptors]) ->
  case is_record(Suscriptor, player) of
    true ->
      case know_him(Suscriptor, Player) of
        true ->
          State2 = ignore_player(Suscriptor, Player, State),
          notify_disconnect_player(Player, State2, Suscriptors);
        false ->
          notify_disconnect_player(Player, State, Suscriptors)
      end;
    false ->
      notify_disconnect_player(Player, State, Suscriptors)
    end.

ignore_player(Player1, Player2, State) ->
  case get_name(Player1) == get_name(Player2) of
    true ->
      State;
    false ->
      State2 = maps:put(get_name(Player1), ignore(Player1, Player2), State),
      State2
  end.

ignore(Player1, Player2) ->
  Player1#player{known_pids = maps:remove(get_name(Player2), Player1#player.known_pids)}.

remove_player(Player, State) ->
  maps:remove(get_name(Player), State).

% Player#player{known_pids= [{Player#player.name, Player#player.pid} | [{SName, self()}]]}.
% Player#player{known_pids = [{Name,Pid}|Player#player.known_pids]}.
update_knows(Player, Name, Pid) ->
  Player#player{known_pids = maps:put(Name, Pid, Player#player.known_pids)}.

% lists:any(fun({Id, _Pid}) -> Id == Suscriptor#player.name end, obj#player.known_pids).
know_him(Player1, Player2) ->
  maps:is_key(get_name(Player2), Player1#player.known_pids).


% ------------------ AGREGANDO PARA APAGAR UN SERVER ------------------

update_interest(Name, Obj,{W,X},{Y,Z}, Suscriptors) ->
    {InterestedRow, InterestedColumn} = calculate_interest(Obj, {W, X}, {Y, Z}),
    maps:put(Name, {Obj, InterestedRow, InterestedColumn}, Suscriptors).

handle_connect_event(Player, SName, {W,X}, {Y,Z}, State, Suscriptors, Transferred) ->
  {UpdatedState, UpdatedPlayer} = add_player(SName, Player, State),
  State2 = notify_new_player(UpdatedPlayer, UpdatedState, maps:values(Suscriptors)),
  Suscriptors2 = update_interest(get_name(UpdatedPlayer), UpdatedPlayer, {W,X}, {Y,Z}, Suscriptors),
  {State2, Suscriptors2}.

handle_new_item_event(SName, Suscriptors, Items, Item) ->
  manager:add(Items, get_name(Item), Item).

handle_new_pos_event(Name, OldPos, NewPos, SName, {W,X}, {Y,Z}, State, Suscriptors, Transferred, Items) ->
  case need_forward(Name, Transferred) of
    true ->
      {State, Suscriptors, Transferred};
    false ->
      case maps:find(Name, State) of
        {ok, Obj} ->
          UpdatedState = maps:remove(Name, State),
          case is_intereseted(NewPos, {W, X}, {Y, Z}) of
            true ->
              {NewObj, State2, Suscriptors2} = change_position_workflow(Obj, OldPos, NewPos, {W,X}, {Y,Z}, State, Suscriptors, Items),
              {State2, Suscriptors2, Transferred};
            false ->
              IServer = select_transfer_event_server(Obj, OldPos, NewPos, maps:values(Suscriptors)),
              Transferred2 = Transferred#{get_name(Obj) => IServer},
              {UpdatedState, Suscriptors, Transferred2}
          end;
        error ->
          io:format("~w no lo encontre! ~w~n", [SName, Name]),
          {State, Suscriptors, Transferred}
      end
  end.

handle_transfer_event(Obj, OldPos, NewPos, SName, {W,X}, {Y,Z}, State, Suscriptors, Items) ->
  {NewObj, State2, Suscriptors2} = change_position_workflow(Obj, OldPos, NewPos,{W,X},{Y,Z}, State, Suscriptors, Items),
  {State2, Suscriptors2}.

change_position_workflow(Obj, OldPos, NewPos, {W,X}, {Y,Z}, State, Suscriptors, Items) ->
  NewObj = change_position(Obj, NewPos),
  State2 = State#{get_name(NewObj) => NewObj},
  NewState = notify_pos_update_with(State2, NewObj, OldPos, NewPos, maps:values(Suscriptors)),
  Suscriptors2 = update_interest(get_name(NewObj), NewObj, {W, X}, {Y, Z}, Suscriptors),
  handle_items(Obj, NewObj, {W,X},{Y,Z}, Items),
  {NewObj, NewState, Suscriptors2}.

handle_items(Obj, NewObj, {W,X},{Y,Z}, Items) when is_record(Obj, player) ->
  {Row, Column} = calculate_interest(NewObj, {W,X}, {Y,Z}),
  {OldRow, OldColumn} = calculate_interest(Obj, {W,X}, {Y,Z}),
  manager:map(Items, fun(IName, Item) ->
    case is_intereseted(get_position(Item), Row, Column) andalso
      not is_intereseted(get_position(Item), OldRow, OldColumn) of
      true -> Item;
      false -> case not is_intereseted(get_position(Item), Row, Column) andalso
        is_intereseted(get_position(Item), OldRow, OldColumn) of
          true ->
            Item;
          false ->
            Item
        end
    end
  end);
handle_items(_,_,_,_,_) ->
  none.

select_transfer_event_server(Obj, OldPos, NewPos, []) -> none;
select_transfer_event_server(Obj, OldPos, NewPos, [{Suscriptor, Row, Column} | Suscriptors]) ->
  case is_record(Suscriptor, server) andalso is_intereseted(NewPos, Suscriptor#server.row, Suscriptor#server.column) of
    true ->
      Suscriptor;
    false ->
      select_transfer_event_server(Obj, OldPos, NewPos, Suscriptors)
  end.
meet_player(Player, Player2, State) ->
  % State2 = maps:put(get_name(Player), Player#player{known_pids = [{Player2#player.name, Player2#player.pid}|Player#player.known_pids]}, State),
  P = update_knows(Player, get_name(Player2), get_pid(Player2)),
  State2 = maps:put(get_name(Player), P, State),
  State2.

add_player(SName, Player, State) ->
  P1 = update_knows(Player, get_name(Player), get_pid(Player)),
  UpdatedPlayer = update_knows(P1, SName, self()),
  % UpdatedPlayer = Player#player{known_pids= [{Player#player.name, Player#player.pid} | [{SName, self()}]]},
  UpdatedState = State#{get_name(UpdatedPlayer) => UpdatedPlayer},
  {UpdatedState, UpdatedPlayer}.

notify_new_player(Player, State, []) -> State;
notify_new_player(Player, State, [{Suscriptor, Row, Column} | Suscriptors])->
  case is_intereseted(Player#player.position, Row, Column) of
    true ->
      case is_record(Suscriptor, player) of
        true ->
          State2 = double_meeting(Suscriptor, Player, State),
          notify_new_player(Player, State2, Suscriptors);
        false ->
          notify_new_player(Player, State, Suscriptors)
      end;
    false ->
      notify_new_player(Player, State, Suscriptors)
  end.

double_meeting(Player, Player2, State) ->
  State2 = meet_player(Player, Player2, State),
  meet_player(Player2, maps:get(Player#player.name, State2), State2).

% {NewState, NewObj} = maps:fold(
% fun(_Key, {Suscriptor, Row, Column}, {UState, UObj}) ->
%   notify_pos_update_with(change_position(UObj, NewPos), Suscriptor, Row, Column, OldPos, NewPos, UState)
% end, {UpdatedState, Obj}, Suscriptors),
notify_pos_update_with(State, _Obj, _OldPos, _NewPos, []) -> State;
notify_pos_update_with(State, Obj, OldPos, NewPos, [{Suscriptor, Row, Column}|Suscriptors]) ->
  case is_intereseted(NewPos, Row, Column) of
    true ->
      case is_record(Suscriptor, player) andalso is_record(Obj, player) andalso
        not know_him(Obj, Suscriptor) of
        true ->
          NewState = double_meeting(Suscriptor, Obj, State),
          notify_pos_update_with(NewState, Obj, OldPos, NewPos, Suscriptors);
        false ->
          notify_pos_update_with(State, Obj, OldPos, NewPos, Suscriptors)
      end;
    false when is_record(Obj, player) ->
      out_of_vision(Obj, Suscriptor),
      case is_intereseted(OldPos, Row, Column) of
        true ->
          notify_pos_update_with(State, Obj, OldPos, NewPos, Suscriptors);
        false ->
          notify_pos_update_with(State, Obj, OldPos, NewPos, Suscriptors)
      end;
    false ->
      case is_intereseted(OldPos, Row, Column) of
        true ->
          notify_pos_update_with(State, Obj, OldPos, NewPos, Suscriptors);
        false ->
          notify_pos_update_with(State, Obj, OldPos, NewPos, Suscriptors)
      end
  end.

out_of_vision(#player{pid= Pid}, #server{}) ->
  none;
out_of_vision(#player{pid= Pid}, Suscriptor) ->
  none;
out_of_vision(_Obj, Suscriptor) ->
  none.

need_forward(Name, Map) ->
  maps:is_key(Name, Map).

create_matrix(_N, _M, _Initial) ->
  maps:new().

calculate_interest(Player, {W, X}, {Y, Z}) ->
  {PX, PY} = Player#player.position,
  InterestedRow = {max(PX - Player#player.visibility, W), min(PX + Player#player.visibility, X)},
  InterestedColumn = {max(PY - Player#player.visibility, Y), min(PY + Player#player.visibility, Z)},
  {InterestedRow, InterestedColumn}.

is_intereseted({A, B}, Row, Column) ->
  in(Row, A) andalso in(Column, B).

part_of(Row, Column, Visibility, {W, X}, {Y, Z}) ->
  in_server({Row - Visibility, Row + Visibility + 1}, {Column - Visibility, Column + Visibility + 1}, {W, X}, {Y, Z}).

in_server({W, X}, {Y, Z}, {WP, XP}, {YP, ZP}) ->
  (in({W, X}, WP) andalso (in({Y, Z}, XP) orelse in({Y, Z}, ZP))) orelse
  (in({W, X}, YP) andalso (in({Y, Z}, XP) orelse in({Y, Z}, ZP))).

in({X, Y}, Z) ->
  X =< Z andalso Z < Y.
