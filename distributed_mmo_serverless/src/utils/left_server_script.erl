-module(left_server_script).
-export([fancion/1, fancion2/7]).

fancion(Ip) ->
Serv1 = server:start(serv1, {0, 384}, {0, 512}),
Serv4 = server:start(serv4, {384, 768}, {512, 1024}),
MS = {main_server, Ip},
GS1 = game_state:start(serv1, {0, 384}, {0, 512}),
GS4 = game_state:start(serv4, {384, 768}, {512, 1024}),
GS2 = {game_state_0_512, Ip},
GS3 = {game_state_384_0, Ip},
Serv1 ! {set_state, GS1},
Serv4 ! {set_state, GS4},
{MS, Serv1, Serv4, GS1, GS2, GS3, GS4}.


fancion2(MS, Serv1, Serv4, GS1, GS2, GS3, GS4) ->
  GS1 ! {neighbours, [{GS2, {0, 384}, {512, 768}}, {GS3, {384, 512}, {0, 512}}, {GS4, {384, 512}, {512, 768}}]},
  GS4 ! {neighbours, [{GS2, {128, 384}, {512, 1024}}, {GS3, {384, 768}, {256, 512}}, {GS1, {128, 384}, {256, 512}}]},
  main_server:add_zone_server(MS, serv1, Serv1),
  main_server:add_zone_server(MS, serv4, Serv4).
