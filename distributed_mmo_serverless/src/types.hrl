-record(player, {name = "", position = {0, 0}, logged = false, visibility = 128, items = [], pid, known_pids}).
-record(server, {name = "", pid, row, column}).

-record(wall, {name = "", position = {0, 0}, fortitude = 5}).

-record(item, {name = "", position = {0, 0}, value = 5}).
-record(rocket, {name = "", position = {0, 0}, damage = 5}).

mk_player(Name, Position, Items, Pid, Known_pids) ->
  #player{name = Name, position = Position, items = Items, pid = Pid, known_pids = Known_pids}.

mk_server(Name, Pid, Row, Column) ->
  #server{name = Name, pid = Pid, row= Row, column = Column}.

mk_item(Position, Value) ->
  Name = list_to_atom(erlang:ref_to_list(make_ref())),
  #item{name=Name, position=Position, value=Value}.

-type suscriptor() :: #player{} | #server{}.
-type positionable() :: #player{} | #item{} | #wall{}.
-type position() :: {integer(), integer()}.



-spec get_pid(suscriptor()) -> identifier().
get_pid(#player{pid= Pid}) -> Pid;
get_pid(#server{pid= Pid}) -> Pid.

-spec get_name(suscriptor()) -> string().
get_name(#player{name= Name}) -> Name;
get_name(#item{name= Name}) -> Name;
get_name(#server{name= Name}) -> Name.

-spec get_position(positionable()) -> position().
get_position(#item{position= Position}) -> Position;
get_position(#wall{position= Position}) -> Position;
get_position(#player{position= Position}) -> Position.

-spec change_position(positionable(), position()) -> positionable().
change_position(P, Pos) when is_record(P, player)-> P#player{position= Pos};
change_position(P, Pos) when is_record(P, item)-> P#item{position= Pos};
change_position(P, Pos) when is_record(P, wall)-> P#wall{position= Pos}.
