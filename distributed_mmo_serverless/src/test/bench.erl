-module(bench).
-export([start/4, stop/1]).


start(ClientSize, MainServer, Servers, Sleep) ->
  spawn(fun() ->
    Clients = create_random_clients(ClientSize, MainServer, Servers, Sleep),
    receive
      stop ->
        lists:foreach(fun(X) -> client:stop(X) end, element(1, Clients))
    end
  end).

create_random_clients(Quantity, MainServer, Servers, Sleep) ->
  Ls = lists:duplicate(Quantity, fun(X) ->
    client:start(list_to_atom("node" ++ integer_to_list(X)), MainServer,lists:nth(random:uniform(length(Servers)), Servers), random:uniform(10321), Sleep)
  end),
  lists:foldl(fun(X, {XS, Acc}) -> C = X(Acc), {[C|XS], Acc + 1} end, {[], 0}, Ls).

stop(Bench) -> Bench ! stop.
