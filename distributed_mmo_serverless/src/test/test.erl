-module(test).
-export([start/0]).

start() ->
  MS = main_server:start(),
  Serv1 = server:start(serv1, {0, 384}, {0, 512}),
  Serv2 = server:start(serv2, {0, 384}, {512, 1024}),
  Serv3 = server:start(serv3, {384, 768}, {0, 512}),
  Serv4 = server:start(serv4, {384, 768}, {512, 1024}),
  main_server:add_zone_server(MS, serv1, Serv1),
  main_server:add_zone_server(MS, serv2, Serv2),
  main_server:add_zone_server(MS, serv3, Serv3),
  main_server:add_zone_server(MS, serv4, Serv4),
  GS1 = game_state:start(serv1, {0, 384}, {0, 512}),
  GS2 = game_state:start(serv2, {0, 384}, {512, 1024}),
  GS3 = game_state:start(serv3, {384, 768}, {0, 512}),
  GS4 = game_state:start(serv4, {384, 768}, {512, 1024}),
  GS1 ! {neighbours, [{GS2, {0, 384}, {512, 768}}, {GS3, {384, 512}, {0, 512}}, {GS4, {384, 512}, {512, 768}}]},
  GS2 ! {neighbours, [{GS1, {0, 384}, {256, 512}}, {GS3, {384, 512}, {256, 512}}, {GS4, {384, 512}, {512, 1024}}]},
  GS3 ! {neighbours, [{GS1, {128, 384}, {0, 512}}, {GS2, {128, 384}, {512, 768}}, {GS4, {384, 768}, {512, 768}}]},
  GS4 ! {neighbours, [{GS2, {128, 384}, {512, 1024}}, {GS3, {384, 768}, {256, 512}}, {GS1, {128, 384}, {256, 512}}]},
  Serv1 ! {set_state, GS1},
  Serv2 ! {set_state, GS2},
  Serv3 ! {set_state, GS3},
  Serv4 ! {set_state, GS4},
  MS.
% B = bench:start(30, MS, [serv1, serv2, serv3, serv4], 30).


start(MS, {N1, Serv1}, {N2, Serv2}, {N3, Serv3}, {N4, Serv4}) ->
  main_server:add_zone_server(MS, N1, Serv1),
  main_server:add_zone_server(MS, N2, Serv2),
  main_server:add_zone_server(MS, N3, Serv3),
  main_server:add_zone_server(MS, N4, Serv4).
