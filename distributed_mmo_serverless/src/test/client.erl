-module(client).
-export([start/5, stop/1, disconnect/1, enclose/2, distance/2]).

-include_lib("types.hrl").

start(Name, MainServer, Server, Seed, Sleep) ->
  spawn(fun() -> init(Name, MainServer, Server, Seed, Sleep) end).

stop(Client) -> Client ! stop.

init(Name, MainServer, Server, Seed, Sleep) ->
  random:seed(Seed, Seed, Seed),
  MainServer ! {register_event, Name, self(), Server},
  receive
    register_successful ->
      MainServer ! {connect_event, Name, self()},
      receive
      {ack_connect, State, PID, Position} ->
        loop(Name, PID, Position, Sleep, random:uniform(Sleep), {0, 0}, none)
      end
  end.

loop(Name, Server, Position, Sleep, T, AvgReplyTime, none) when T > 0 ->
  T1 = erlang:system_time(micro_seconds),
  receive
    {meet_player, State} ->
      loop(Name, Server, Position, Sleep, T - erlang:system_time(micro_seconds) + T1, AvgReplyTime, none);
    {out_of_vision, OName, _OldPos} ->
      loop(Name, Server, Position, Sleep, T - erlang:system_time(micro_seconds) + T1, AvgReplyTime, none);
    {change_pos, OName, Obj, _OldPos, NewPos} when OName == Name ->
      loop(Name, Server, NewPos, Sleep, T - erlang:system_time(micro_seconds) + T1, AvgReplyTime, none);
    {change_pos, OName, Obj, _OldPos, _NewPos} ->
      loop(Name, Server, Position, Sleep, T - erlang:system_time(micro_seconds) + T1, AvgReplyTime, none);
    {new_server, Pid} ->
      Pid ! {ack_transfer, Name, Server},
      loop(Name, Pid, Position, Sleep, T - erlang:system_time(micro_seconds) + T1, AvgReplyTime, none);
    {new_item, Item} ->
      loop(Name, Server, Position, Sleep, T - erlang:system_time(micro_seconds) + T1, AvgReplyTime, Item);
    {ignore, Player} ->
      io:format("~w DEJA DE CONOCER A: ~w~n", [Name, get_name(Player)]),
      loop(Name, Server, Position, Sleep, T - erlang:system_time(micro_seconds) + T1, AvgReplyTime, none);
    disconnect_event ->
      Server ! {disconnect_event, Name},
      ok;
    stop ->
      io:format("~w Avg reply time: ~w~n", [Name, element(1,AvgReplyTime) / element(2, AvgReplyTime)])
  after T ->
      move(Name, Server, Position, Sleep, AvgReplyTime, none)
  end;
loop(Name, Server, Position, Sleep, T, AvgReplyTime, Item) when T > 0 ->
  T1 = erlang:system_time(micro_seconds),
  receive
    {meet_player, State} ->
      loop(Name, Server, Position, Sleep, T - erlang:system_time(micro_seconds) + T1, AvgReplyTime, Item);
    {out_of_vision, OName, _OldPos} ->
      loop(Name, Server, Position, Sleep, T - erlang:system_time(micro_seconds) + T1, AvgReplyTime, Item);
    {change_pos, OName, Obj, _OldPos, NewPos} when OName == Name ->
      loop(Name, Server, NewPos, Sleep, T - erlang:system_time(micro_seconds) + T1, AvgReplyTime, Item);
    {change_pos, OName, Obj, _OldPos, _NewPos} ->
      loop(Name, Server, Position, Sleep, T - erlang:system_time(micro_seconds) + T1, AvgReplyTime, Item);
    {new_server, Pid} ->
      Pid ! {ack_transfer, Name, Server},
      loop(Name, Pid, Position, Sleep, T - erlang:system_time(micro_seconds) + T1, AvgReplyTime, Item);
    {new_item, _} ->
      loop(Name, Server, Position, Sleep, T - erlang:system_time(micro_seconds) + T1, AvgReplyTime, Item);
    {ignore, Player} ->
      io:format("~w DEJA DE CONOCER A: ~w~n", [Name, get_name(Player)]),
      loop(Name, Server, Position, Sleep, T - erlang:system_time(micro_seconds) + T1, AvgReplyTime, Item);
    disconnect_event ->
      Server ! {disconnect_event, Name},
      ok;
    stop ->
      io:format("~w Avg reply time: ~w~n", [Name, element(1,AvgReplyTime) / element(2, AvgReplyTime)])
  after T ->
      move(Name, Server, Position, Sleep, AvgReplyTime, Item)
  end;
loop(Name, Server, Position, Sleep, _T, AvgReplyTime, none) ->
  move(Name, Server, Position, Sleep, AvgReplyTime, none);
loop(Name, Server, Position, Sleep, _T, AvgReplyTime, Item) ->
  move(Name, Server, Position, Sleep, AvgReplyTime, Item).

move(Name, Server, Position, Sleep, AvgReplyTime, none) ->
  NewPos = change_position(Position),
  Server ! {new_pos_event, Name, Position, NewPos},
  waiting_ack_move(Name, Server, Position, Sleep, erlang:system_time(milli_seconds), AvgReplyTime, none);
move(Name, Server, Position, Sleep, AvgReplyTime, Item) ->
  NewPos = enclose(Position, get_position(Item)),
  Server ! {new_pos_event, Name, Position, NewPos},
  waiting_ack_move(Name, Server, Position, Sleep, erlang:system_time(milli_seconds), AvgReplyTime, Item).

waiting_ack_move(Name, Server, Position, Sleep, Start, {X, Y}, none) ->
  receive
    {meet_player, State} ->
      waiting_ack_move(Name, Server, Position, Sleep, Start, {X, Y}, none);
    {out_of_vision, OName, _OldPos} ->
      waiting_ack_move(Name, Server, Position, Sleep, Start, {X, Y}, none);
    {change_pos, OName, Obj, _OldPos, NewPos, Pid} when OName == Name ->
      loop(Name, Server, NewPos, Sleep, random:uniform(Sleep), {X + (erlang:system_time(milli_seconds) - Start), Y + 1}, none);
    {change_pos, OName, Obj, _OldPos, _NewPos, _Pid} ->
      waiting_ack_move(Name, Server, Position, Sleep, Start, {X, Y}, none);
    {new_server, Pid} ->
      Pid ! {ack_transfer, Name, Server},
      waiting_ack_move(Name, Pid, Position, Sleep, Start, {X, Y}, none);
    {new_item, Item} ->
      % io:format("~w: Hola nuevo item ~w~n", [Name, Item]),
      waiting_ack_move(Name, Server, Position, Sleep, Start, {X, Y}, Item);
    {ignore, Player} ->
      waiting_ack_move(Name, Server, Position, Sleep, Start, {X,Y}, none);
    disconnect_event ->
      Server ! {disconnect_event, Name},
      ok;
    stop ->
      io:format("~w Avg reply time: ~w~n", [Name, X / Y])
  end;
waiting_ack_move(Name, Server, Position, Sleep, Start, {X, Y}, Item) ->
  receive
    {meet_player, State} ->
      waiting_ack_move(Name, Server, Position, Sleep, Start, {X, Y}, Item);
    {out_of_vision, OName, _OldPos} ->
      waiting_ack_move(Name, Server, Position, Sleep, Start, {X, Y}, Item);
    {change_pos, OName, Obj, _OldPos, NewPos, Pid} when OName == Name ->
      case distance(Position, get_position(Item)) < 21 of
        true ->
          Pid ! {pick_up_event, Name, get_name(Item)},
          loop(Name, Server, NewPos, Sleep, random:uniform(Sleep), {X + (erlang:system_time(milli_seconds) - Start), Y + 1}, none);
        false ->
          loop(Name, Server, NewPos, Sleep, random:uniform(Sleep), {X + (erlang:system_time(milli_seconds) - Start), Y + 1}, Item)
      end;
    {change_pos, OName, Obj, _OldPos, _NewPos, _Pid} ->
      waiting_ack_move(Name, Server, Position, Sleep, Start, {X, Y}, Item);
    {new_server, Pid} ->
      Pid ! {ack_transfer, Name, Server},
      waiting_ack_move(Name, Pid, Position, Sleep, Start, {X, Y}, Item);
    {new_item, _} ->
      % io:format("~w: Hola nuevo item ~w~n", [Name, Item]),
      waiting_ack_move(Name, Server, Position, Sleep, Start, {X, Y}, Item);
    {ignore, Player} ->
      waiting_ack_move(Name, Server, Position, Sleep, Start, {X,Y}, Item);
    disconnect_event ->
      Server ! {disconnect_event, Name},
      ok;
    stop ->
      io:format("~w Avg reply time: ~w~n", [Name, X / Y])
  end.

change_position({X, Y}) ->
  {min(max(0, X + (random:uniform() * 2 - 1)), 1023), max(0, min(Y + (random:uniform() * 2 - 1), 1023))}.
enclose({X,Y}, Pos) ->
  {NX, NY} = {random:uniform(), random:uniform()},
  case distance({X + NX, Y}, Pos) < distance({X, Y}, Pos) of
    true ->
      {X + NX, Y};
    false ->
      case distance({X - NX, Y}, Pos) < distance({X, Y}, Pos) of
        true ->
          {X - NX, Y};
        false ->
          case distance({X, Y - NY}, Pos) < distance({X, Y}, Pos) of
            true ->
              {X, Y - NY};
            false ->
              {X, Y + NY}
          end
      end
  end.

distance({W, X}, {Y, Z}) ->
  math:sqrt(math:pow(W - Y, 2) + math:pow(X - Z, 2)).

disconnect(Client) ->
  Client ! disconnect_event.
