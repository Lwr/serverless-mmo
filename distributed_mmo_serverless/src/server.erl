-module(server).
-export([start/3, start/4]).

-include_lib("types.hrl").

% Row and Column are tuples {Int, Int}
start(Name, Row, Column) ->
  Pid = spawn_link(fun() -> init(Name, Row, Column, main_server, manager:new()) end),
  register(Name, Pid),
  Pid.

start(Name, Row, Column, MServer) ->
  Pid = spawn_link(fun() -> init(Name, Row, Column, MServer, manager:new()) end),
  register(Name, Pid),
  Pid.

init(Name, Row, Column, MServer, Players) ->
  receive
    {set_state, State} ->
      Ref = erlang:monitor(process, State),
      loop(Name, Row, Column, Players, State, MServer, Ref)
  end.

loop(ServerName, {W, X}, {Y, Z}, Players, State, MServer, Ref) ->
  receive
    {register_event, Name, Pid} ->
      Player = mk_player(Name, {W + random:uniform(X - W), Y + random:uniform(Z - Y)}, [], Pid, maps:put(ServerName, self(), maps:new())),
      manager:add(Players, Name, Player),
      Pid ! register_successful,
      loop(ServerName, {W, X}, {Y, Z}, Players, State, MServer, Ref);
    {connect_event, Name, Pid} ->
      manager:update(Players, Name,
        log_player(Name, Pid, State)
      ),
      loop(ServerName, {W, X}, {Y, Z}, Players, State, MServer, Ref);
    {disconnect_event, Player} ->
      io:format("DESLOGEANDO: ~w FROM SERVER: ~w~n", [get_name(Player), ServerName]),
      manager:add(Players, get_name(Player), Player#player{pid = none, logged = false}, deslog_player(ServerName, Player, MServer)),
      loop(ServerName, {W,X}, {Y,Z}, Players, State, MServer, Ref);
    stop ->
      State ! stop,
      ok;
    {'DOWN', Ref, process, Pid, Reason} ->
      init(ServerName, {W, X}, {Y, Z}, MServer, Players)
  end.

%%%%%%%%%%%%%%%%%%%%%
% Private functions %
%%%%%%%%%%%%%%%%%%%%%
log_player(_Name, Pid, State) ->
  fun (Player) ->
    UpdatedPlayer = Player#player{pid = Pid, logged = true},
    State ! {connect_event, UpdatedPlayer},
    UpdatedPlayer
  end.

deslog_player(ServerName, Player, MServer) ->
  fun() ->
    MServer ! {disconnect_player, get_name(Player), ServerName}
  end.
