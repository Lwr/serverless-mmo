Una arquitectura genérica de MMORPG (Massive multiplayer online role-playing game)
=====

<sup>Este proyecto esta basado en [este paper](https://pdos.csail.mit.edu/archive/6.824-2005/reports/assiotis.pdf)</sup>

# How to run the benchmark

```shell
$ rebar3 shell -sname server
```

```erlang
MS = test:start().
NumberOfClients = 30.
SleepTime = 30.
B = bench:start(NumberOfClients, MS, [serv1, serv2, serv3, serv4], SleepTime).
% wait as much as you want
bench:stop().
```


# Marco teórico

## MMORPG

Si estás familiarizado con los mmorpgs podes saltear esta sección.

### Mundo Virtual

Los MMORPG tratan de simular la vida real tanto como sea posible. Para conseguir esto hay que evolucionar el mundo constantemente usando un conjunto de leyes. Éstas leyes son un conjunto complejo de reglas que el motor del juego aplica en cada ciclo de reloj. Un ejemplo trivial de reglas de este estilo es un personaje caminando cerca de un NPC (non-player character) que como consecuencia empieza a atacarlo.
El mundo virtual consiste no solo en jugadores humanos, sino también de todos los objetos que lo habitan. Algunos son inmutables como los árboles, las piedras, etc, y otros cambian según las acciones que toman los jugadores humanos, y hasta a veces los NPC.

### Jugador

Un jugador es definido como un humano que interactúa de forma activa con el mundo virtual, controlando un solo personaje que lo representa como un avatar. Cada personaje generalmente tiene un estado único que consiste en muchas propiedades las cuales típicamente son vida, agilidad, fuerza, etc. En un juego típico los jugadores tienen que seguir diferentes misiones, quests, o simplemente aumentar de nivel, recorriendo diferentes partes del mundo virtual.

### Personaje no-jugador (PNJ o NPC en inglés)

Suelen ser monstruos, vendedores, o incluso hasta personajes como los de los humanos, controlados por software _(aka Artificial intelligence o AI)_. Los jugadores interactúan cotidianamente con los NPC como si fueran otros Jugadores.

### Eventos

Un evento es una acción que afecta al mundo virtual y que usualmente cambia su estado. Algunos ejemplos son: un jugador caminando, un jugador canalizando un hechizo o cargando un arma para disparar. Estas acciones tienen consecuencias directas o indirectas en el mundo, por ejemplo un jugador caminando cambia su propio estado que es un subconjunto del estado del mundo. Los eventos contienen toda la información que requiere el motor del juego para ejecutarlo.

### Tolerancia a fallos

#### Master-Slave

Consiste en la idea de tener un proceso que controla uno o varios procesos delegando ciertas tareas.
En nuestro caso, se nos ocurrió seguir esta idea para mantener una copia de los
eventos que ocurren en los game state.

Como ventajas tenemos que habiendo n servidores de zona, tendrían que fallar los n masters y un slave para que el sistema deje de estar disponible (CAP theorem).

La principal desventaja es que se dados n servidores de zona, necesitamos n servidores extra para los slaves que mantienen las réplicas.

#### Overlapping areas y Slave on subscription

Estos dos consisten en que hayan áreas que están cubiertas por más de 1 servidor con el objetivo de mitigar las posibles fallas en los servidores que fallen en esas zonas, de manera tal que si se cae alguno de los otros servidores se hará cargo de esa área.

El slave on subscription es un subset de overlapping areas, ya que este utiliza además la suscripción como ventaja para poder replicar la información de los servidores a los que está suscrito.


### Área de interés

Es el área que afecta a un jugador en particular. (_e.g:_ Suponiendo que el juego transcurre en un mapa basado en Bernal, si un jugador toma un item del piso en la estación, no le afecta en nada a otro jugador que está cursando la materia sistemas distribuidos en la UNQ).

Esto es muy importante ya que se puede explotar esta idea para no tener que notificar a todos los usuarios del sistema sobre un evento que no les interese.


## Tipos de arquitectura:

* **Cliente/servidor**: En esta arquitectura un único server se encarga de mantener todo el estado del juego y de atender a todos los clientes. Se usa en algunos First Person Shooter (_a.k.a_: FPS) como _Quake_ y _Doom_.
Es claro que esta arquitectura no escala mucho ya que el servidor funciona como un cuello de botella.

* **Cliente/servidor espejado**: Es similar al tipo Cliente/servidor, salvo que con muchos servidores replicados que se dividen el trabajo de atender a los clientes. Aunque hay algunos métodos sofisticados y complejos para mantenerlos sincronizados en algún punto se vuelve muy complicado mantener la consistencia entre todos los servers en un entorno tan variable como el de los MMORPGs con cientos y hasta a veces miles de jugadores al mismo tiempo.

* **Peer-to-peer**(_a.k.a_: P2P): Este approach es muy interesante porque aprovecha el principio de la localidad del área de interés (como en la arquitectura que explicaremos a lo largo de este documento). Sin embargo en los sistemas P2P el estado del juego se mantiene en los clientes, siendo cada cliente responsable de una pequeña región: Los clientes hacen un multicast para actualizar las regiones de los demás peers, lo que hace que el overhead de bandwidth sea mayor al de las arquitecturas explicadas anteriormente. Otro de los problemas que presenta es que al delegarle la tarea de mantener la consistencia y el estado a los clientes se requiere una computadora muy potente para correr el juego en cuestión, por lo que esto llevaría a la pérdida de potenciales jugadores. Por último es más fácil encontrar cheaters en juegos con esta arquitectura, ya que al tener el estado localmente, es probable que se intente modificarlo sin consultar con los demás pares.

# Dificultades y objetivos

Algunos de los objetivos que se enfrentan a la hora de diseñar un MMORPG son:

* La capacidad de atender un gran número de usuarios simultáneos. Como la información transferida entre usuarios y el servidor del juego es grande, el ancho de banda requerido para soportar todo esto es enorme.
* Muchos mundos virtuales grandes requieren mucho poder de cómputo para simular la existencia de vida. Ningún proceso solo puede con todo el poder computacional requerido.

Como explicaremos luego la arquitectura consiste en separar el mundo virtual en pequeñas subáreas del mismo, cada una en una computadora distinta. Por esta razón la banda ancha y la carga computacional son separadas entre varias computadoras.

# Detalles de la implementación

Siguiendo la idea del paper nos surgió la necesidad de implementar un <small><small><small>mini</small></small></small> hash distribuido que en nuestro llamamos **manager**, cuya interfaz es:

```erlang
-module(manager).
-export([new/0, add/3, add/4, update/3, remove/2, find/4, do_when/3, do_when/4, remove_when/4, map/2, stop/1]).
```

Nuestra implementación de la arquitectura consiste básicamente en 3 módulos que son:

* MainServer
* Server
* GameState

Cuyas responsabilidades son:

## Main Server

Es el encargado de manejar la asignación de nombres o ids a pids de los servidores de zonas. Además mantiene la información del último servidor en el que cada jugador se desconectó. Por ejemplo si el jugador ___XxDestructorxX___ se desconecta estando en el servidor 28, luego de terminar la persistencia de todos los datos del jugador, el main_server recibirá la notificación de desconección del jugador en ese server, con lo que persistirá esta información, para poder redirigirlo en su próxima conexión.

## Server

Es el encargado del layer de persistencia, que en nuestro caso sucede
cuando se desconecta un jugador, por otro lado se encarga de hacer la conexión entre el server y el game state. Cuando se conecta un jugador, recibe una notificación del main_server para conectarlo. Para proceder a conectarlo, el server busca los datos del jugador, y los modifica con el process id actual y el tag logged in. Luego notifica al game_state que se encargará de notificar y enviar el estado inicial al jugador.

## Game State

Es el encargado de recibir las request de los clientes, manejar las subscripciones, manejar los diferentes eventos que suceden en el servidor y pasar la información a los servidores y clientes suscritos.
Algunos de los eventos que pueden ocurrir en el game state son los siguientes:
  * connect_event
  * disconnect_player_event
  * new_pos_event
  * new_item_event
  * pick_up_event

### Ejemplo de flujo de un evento:

* new pos event:

```erlang
{new_pos_event, Name, OldPos, NewPos} ->
  {State2, Suscriptors2, Transferred2} = handle_new_pos_event(Name, OldPos, NewPos, SName, {W,X}, {Y,Z}, State, Suscriptors, Transferred, Items),
  loop(SName, {W,X}, {Y,Z}, State2, Suscriptors2, Transferred2, Items).
```

Para mantener las cosas simples, vamos a decir que handle_new_pos_event/11 tiene tres posibilidades, si ese evento hace que quien sea que lo está mandando deba ser transferido, que lo tenga que handlear efectivamente, o que ya lo haya transferido pero que está en la lista para ser forwardeado. En el caso en que lo tenga que handlear el servidor, este tendrá que avisar a todos los subscriptores a los que les interese esa posición que esa entidad cambió de posición, tanto como a los suscriptores que ***ya no les interese*** que __Name__ salió de su rango de visión. Además tiene que actualizar el área de interés de la entidad esa, si es que esta suscrita.

## Luego de la presentación

Luego de finalizar la presentación pudimos implementar Master-slave explicado anteriormente. La implementación principalmente consistió en un módulo que hace lo mismo que el game_state salvo que no notifica a nadie de las cosas que van pasando. De esta forma puede mantener el estado replicado del servidor en cuestión.

Por otro lado los slaves reciben por parámetro los bounds de la parte de la que son slaves, por lo que es fácil lograr que hayan varios slaves cuidando a un mismo servidor.

Lo bueno de haber implementado esto es que los jugadores casi no notaran si se cae un servidor. La tolerancia a fallos va hasta todos los servidores masters. Es decir que tienen que fallar todos los servidores más uno para que recién lo note el usuario final.

Otra de las ventajas que provee esta implementación es la capacidad de tirar un servidor para mantenimiento, para luego ponerlo como slave o master nuevamente.

# Bench

Implementamos un bench que de forma paramétrica permite instanciar en diferentes nodos, una cantidad arbitraria de clientes que le peguen al servidor, moviéndose de forma aleatoria hasta encontrar un item cerca, para luego intentar agarrar el item, utilizando la menor distancia euclidiana.
Los clientes van tomando estadísticas de los tiempos de respuesta del servidor para reportarlas cuando se les dice.

### Resultados

* 40 jugadores mandando requests cada 30ms c/u - 4 servidores - virtual world de 1024x768 - cada servidor con ¼ del mapa:  0.2ms de tiempo de respuesta promedio

* 100 jugadores mandando requests cada 30ms c/u - 4 servidores - virtual world de 1024x768 - cada servidor con ¼ del mapa:  14ms de tiempo de respuesta promedio

# Prueba de integración y carga

Para realizar la prueba de integración y carga, implementamos un cliente hecho con libgdx en java. Para lo cual tuvimos que utilizar la interfaz entre erlang y java llamada jinterface.

La verdad la interfaz deja mucho que desear en cuanto diseño, ya que todo está wrapeado en ErlangObject, por lo cual hay que castear **todo**. Por otro lado no se puede instanciar monitores entre nodos de erlang y nodos de java, por lo cual es difícil hacer una conexión segura.

Sin embargo la experiencia de meterse un poquito en la interfaz entre lenguajes completamente diferentes estuvo muy buena.

# Conclusión

En cuanto a la implementación, creemos que erlang resuelve muchas cosas de manera simple con el modelo de procesos que tiene. Por otro lado, nos hubiera gustado implementar esto en otro lenguaje como C++ para comparar performance.

En cuanto al diseño de la arquitectura propuesta por el paper, fue bastante
difícil llevar a cabo muchas ideas que no están bien detalladas como por ejemplo
el manejo de la transferencia entre servidores.

Por último fue muy divertido enfrentarse con un problema de carácter distribuido que nunca antes habìamos intentado resolver, además de la implementación de un cliente java para hacer la prueba de integración.

# Trabajo futuro

Habiendo implementado una interfaz gráfica que interactúa con los servidores de forma transparente, una de las cosas que se puede plantear hacer es la implementación y puesta en marcha de un juego en serio, con servidores potentes para así descubrir las cualidades de la arquitectura de una forma empírica más cercana a un ambiente de producción.

La comparación con la misma implementación hecha en otro lenguaje es indispensable para ver si es factible utilizar erlang para este tipo de desarrollos.

Por último, esta arquitectura se puede extender para cualquier tipo de juego online por ejemplo First person shooter. Se podrían hacer pruebas sobre alguna implementación de este estilo.
